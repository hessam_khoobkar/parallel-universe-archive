# Parallel Universe Archive

Welcome to the Parallel Universe Archive, a showcase project highlighting my top 100 recommended video games. This project is a testament to my front-end development skills, utilizing technologies like Next.js, Tailwind CSS, and Supabase for the backend. Framer Motion has been employed to enhance the user experience with advanced animations and micro-interactions.

## Project Overview

**Deployment:**
The project is live and accessible through my [website](https://khoobkar.com/) or directly at [here](https://parallel-universe-archive.vercel.app/).\
To run the development server in your local environment:\
fork the project install all the dependencies with `npm install` and run the development and server with `pnpm dev`.

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Technologies Used

### Frontend

**Next.js:** A React framework for building server-side rendered and statically generated web applications.\
**Tailwind CSS:** A utility-first CSS framework for building modern and responsive designs.\
**Framer Motion:** Used for implementing advanced animations and micro-interactions.

### Backend

**Supabase:** A powerful open-source platform that combines the best features of databases and serverless functions for building applications.

## To-Dos

- [ ] Implement Queue Page
Create a dedicated page to manage and display the queue of video games I'm yet to play.

- [ ] Global State for Data Fetching
Enhance data fetching by integrating a global state management solution. This will improve the overall efficiency and maintainability of data retrieval.

- [ ] Modularize and Separate Functions
Refactor code to ensure functions are modular and well-separated. This will enhance code readability, maintainability, and make future enhancements easier.

- [ ] Testing Implementation
Introduce testing to the application to ensure its reliability and robustness. This includes unit testing for individual components and integration testing for overall functionality.

- [ ] Implement responsive design

## How to Contribute

If you'd like to contribute to the Parallel Universe Archive project, feel free to fork the repository and submit a pull request with your proposed changes. Additionally, bug reports and feature requests are always welcome through the GitHub issues page.

*Happy gaming, and enjoy exploring the Parallel Universe Archive!*
